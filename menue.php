<script src="assets/js/xhr.js"></script>
<script src="assets/js/jquery-2.1.4.js"></script>
<script>
    $(document).ready(function () {
        main();
    });


    var contador = 1;

    function main(){
        $('#menu').click(function(){
            // $('nav').toggle();
            if(contador == 1){
                $('.mover').animate({
                    left: '0'
                });
                contador = 0;
            } else {
                contador = 1;
                $('.mover').animate({
                    left: '-100%'
                });
            }

        });

    };
</script>
<script type="text/javascript">

	function setIdioma(valor){
			var peticion=Request();
        if (peticion) {
            var url="./idioma.php?var="+valor;
            peticion.open("GET",url,false);
            peticion.setRequestHeader("Content-type","application/x-www-form-urlencoded");
            peticion.send(null);

            location.reload();
        }
	}
</script>
<header class="vistas">
    <a id="menu" class="navPanelToggle"><span class="fa fa-bars"></span></a>

    <nav class="mover" style="z-index: 2">
        <ul>
            <li><a href="our-work.php"><span class="icon-house"></span>Nuestro trabajo</a></li>
            <li><a href="sobre-mi.php"><span class="icon-suitcase"></span>Sobre mi</a></li>
            <li><a href="contacto.php"><span class="icon-rocket"></span>Contactános</a></li>
            <li><a href="faq.php"><span class="icon-earth"></span>FAQ</a></li>
        </ul>
    </nav>
</header>
<header id="header" style="position:absolute; z-index:1;height: 100px" >
	<nav id="movimiento">
        <ul class="lengua" onclick="setIdioma(1)">
            <img src="images/mexico.png"  class="mexico">
        </ul>
		<div class="inner">
            <div id="nav" style="float: left;margin-left: 18em">
                <a href="our-work.php">Nuestro trabajo</a>
                <a href="sobre-mi.php">Sobre mi</a>
            </div>
            <div>
                <a href="index.php" class="logo"><img src="./images/Logotipo.webp" style="margin-top: .5em;margin-left: -0.5em"></a>
            </div>
            <div id="nav" style="float: right;margin-right: 22.5em">
					<a href="contacto.php">Contactános</a>
					<a href="faq.php">FAQ</a>
            </div>
		</div>
	</nav>
</header>
