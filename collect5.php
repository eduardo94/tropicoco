<?php
session_start();
$doc=$_SESSION["valor"];
?>
<!DOCTYPE html>
<html>
<head>
    <?php if($doc=="1"){?>
        <title>FOTOGRAFÍA | TROPICOCO MEDIA</title><?php
    }
    else{?>
        <title>PHOTOGRAPHY | TROPICOCO MEDIA</title><?php
    } ?>

    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <link rel="stylesheet" href="assets/css/main.css" />
</head>
<body style="overflow-x: hidden;">
<!-- Header -->
<?php if($doc==1){
    include "menue.php";
}
else{
    include "menui.php";
} ?>

<!-- Contenido -->
<section id="contenedoralb">
    <section>
        <div >
            <figure class="coleccion1"><img src="./images/collect5-1.webp"></figure>
            <figure class="coleccion2"><img src="./images/collect4-2.webp"></figure>
        </div>
    </section>
    <section>
        <div >
            <figure class="coleccion3"><img src="./images/collect5-3.webp"></figure>
        </div>
    </section>
    <section>
        <div >
            <figure class="coleccion1"><img src="./images/collect5-4.webp"></figure>
            <figure class="coleccion2"><img src="./images/collect5-5.webp"></figure>
        </div>
    </section>
</section>


<footer id="footwork" class="topito">
    <section>
        <div class="sociales"><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
        <div class="sociales"><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></div>
        <div class="sociales"><a href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube"></i></a></div>
        <div class="sociales"><a href="https://www.instagram.com/tropicocomedia/" target="_blank"><i class="fab fa-instagram"></i></a></div>
    </section>
</footer>


<!-- Scripts -->
<script src="assets/js/jquery-2.1.4.js"></script>
<script src="https://kit.fontawesome.com/14f2e7ba0b.js" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="assets/js/sweetalert.js"></script>
<script type="text/javascript">
    var principal = window.pageYOffset;
    var desplazamiento = 0;
    window.onscroll = function(){
        desplazamiento += window.pageYOffset;
        if (principal>=desplazamiento) {
            document.getElementById("header").style.top = '0';
            document.getElementById("movimiento").style.top = '0';
        }else{
            document.getElementById("header").style.top = '-100px';
            document.getElementById("movimiento").style.top = '-100px';
        }
        principal= desplazamiento;
    }
</script>
</body>
</html>