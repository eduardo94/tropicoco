<?php
	session_start();	
	$doc=$_SESSION["valor"];
?>
<!DOCTYPE html>
<html>
	<head>
		<?php if($doc=="1"){?>
					<title>FAQ | TROPICOCO MEDIA</title><?php
				}
				else{?>
					<title>FAQ | TROPICOCO MEDIA</title><?php
				} ?>
		
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link href="https://fonts.googleapis.com/css2?family=Caveat&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>
		<!-- Header -->
			<?php if($doc==1){
					include "menue.php";
				}
				else{
					include "menui.php";
				} ?>

		<!-- Contenido -->
				<section id="FAQ" style="padding-top: 7%">
					<article id="titulofaq">
						<div>
							<h4>FAQ</h4>
						</div>
					</article>
					<article class="preguntas" onclick="mostrarOc('1')">
						<?php 
							if ($doc==1) {
								echo ' 
									<div class="content">
										<h4>¿Cómo puedo agendar una sesión?</h4><span class="pdown" id="pdown1"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup1"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="1">
										<p>Escribenos con tus intereses en particular y en seguida nos ponemos en contacto contigo.</p>
									</div>
								';	
							}else{
								echo ' 
									<div class="content">
										<h4>How can I schedule a session?</h4><span class="pdown" id="pdown1"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup1"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="1">
										<p>Write to us with your particular interests and we will get in touch with you right away.</p>
									</div>
								';	
							}
						?>
					</article>
					<article class="preguntas" onclick="mostrarOc('2')">
						<?php 
							if ($doc==1) {
								echo '
									<div class="content">
										<h4>¿Hacen bodas?</h4><span class="pdown" id="pdown2"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup2"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="2">
										<p>No cubrimos bodas, pero hacemos sesiones de novios y pre weddings.</p>
									</div>
								 ';
							}else{
								echo '
									<div class="content">
										<h4>Do you do weddings?</h4><span class="pdown" id="pdown2"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup2"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="2">
										<p>We do not cover weddings, but we do wedding sessions and pre weddings.</p>
									</div>
								 ';
							}
						 ?>
						
					</article>
					<article class="preguntas" onclick="mostrarOc('3')">
						<?php 
							if ($doc==1) {
								echo '
									<div class="content">
										<h4>¿Ustedes se encargan de todo?</h4><span class="pdown" id="pdown3"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup3"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="3">
										<p>Nosotros nos hacemos cargo de todos los aspectos de la producción a menos de ser solicitado de alguna manera diferente.</p>
									</div>
								 ';
							}else{
								echo '
									<div class="content">
										<h4>Do you take care of everything?</h4><span class="pdown" id="pdown3"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup3"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="3">
										<p>We take care of all aspects of production unless requested in some other way.</p>
									</div>
								 ';
							}
						 ?>
					</article>
					<article class="preguntas" onclick="mostrarOc('4')">
						<?php 
							if ($doc==1) {
								echo '
									<div class="content">
										<h4>¿Cuánto cuesta una sesión?</h4><span class="pdown" id="pdown4"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup4"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="4">
										<p>Los precios varían dependiendo del concepto, lugar y equipo a necesitar, sin embargo nuestro costos comienzan en $6,500.00 MXN.</p>
									</div>
								';
							}else{
								echo '
									<div class="content">
										<h4>How much does a session cost?</h4><span class="pdown" id="pdown4"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup4"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="4">
										<p>Prices vary depending on the concept, place and equipment to be needed, however our costs start at $ 325.00 USD.</p>
									</div>
								';
							}
						 ?>
						
					</article>
					<article class="preguntas" onclick="mostrarOc('5')">
						<?php 
							if ($doc==1) {
								echo '
									<div class="content">
										<h4>¿Tienen descuentos o promociones?</h4><span class="pdown" id="pdown5"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup5"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="5">
										<p>Sí, contamos con descuentos dependiendo del tamaño de producción.</p>
									</div>
								';
							}
							else{
								echo '
									<div class="content">
										<h4>Do you have discounts or promotions?</h4><span class="pdown" id="pdown5"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup5"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="5">
										<p>Yes, we have discounts depending on the size of production.</p>
									</div>
								';
							}
						 ?>
						
					</article>
					<article class="preguntas" onclick="mostrarOc('6')">
						<?php 
							if ($doc==1) {
								echo '
									<div class="content">
										<h4>¿Hacen fotografía editorial?</h4><span class="pdown" id="pdown6"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup6"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="6">
										<p>Sí, hacemos fotografía editorial, fotografía de deporte, retrato y fotografía de experiencias.</p>
									</div>
								';
							}else{
								echo '
									<div class="content">
										<h4>Do you do editorial photography?</h4><span class="pdown" id="pdown6"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup6"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="6">
										<p>Yes, we do editorial photography, sports photography, portrait photography and experience photography.</p>
									</div>
								';
							}
						 ?>
						
					</article>
					<article class="preguntas" onclick="mostrarOc('7')">
						<?php 
							if ($doc==1) {
								echo '
									<div class="content">
										<h4>¿Dónde se encuentran ubicados y pueden venir a mi ubicación?</h4><span class="pdown" id="pdown7"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup7"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="7">
										<p>Nuestro estudio está en Mérida pero contamos con la movilidad para trasladarnos a la ubicación requerida mientras los viáticos sean cubiertos.</p>
									</div>
								';
							}else{
								echo '
									<div class="content">
										<h4>Where are they located and can they come to my location?</h4><span class="pdown" id="pdown7"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup7"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="7">
										<p>Our studio is in Mérida but we have the mobility to move to the required location while travel expenses are covered.</p>
									</div>
								';
							}
						 ?>
						
					</article>
					<article class="preguntas" onclick="mostrarOc('8')">
						<?php 
							if ($doc==1) {
								echo '
									<div class="content">
										<h4>¿Qué los diferencia a ustedes?</h4><span class="pdown" id="pdown8"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup8"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="8">
										<p>Nuestro trabajo nace de la pasón por crear y siempre vamos más allá de lo que el cliente busca. Para nosotros, cumplir no es suficiente, buscamos sobresalir y que la experiencia sea agradable.</p>
									</div>
								';
							}else{
								echo '
									<div class="content">
										<h4>What differentiates you?</h4><span class="pdown" id="pdown8"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup8"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="8">
										<p>Our work is born from the passion to create and we always go beyond what the client is looking for. For us, complying is not enough, we seek to stand out and make the experience pleasant.</p>
									</div>
								';
							}
						 ?>
						
					</article>
					<article class="preguntas" onclick="mostrarOc('9')">
						<?php 
							if ($doc==1) {
								echo '
									<div class="content">
										<h4>¿Trabajan con equipos grandes de producción?</h4><span class="pdown" id="pdown9"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup9"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="9">
										<p>Sí, trabajamos con equipos con áreas especializadas así como también con equipos pequeños. Nos gusta ser parte de un equipo.</p>
									</div>
								';
							}else{
								echo '
									<div class="content">
										<h4>Do you work with large production teams?</h4><span class="pdown" id="pdown9"><i class="fas fa-angle-down"></i></span>
										<span class="pup" id="pup9"><i class="fas fa-angle-up"></i></span>
									</div>
									<div class="respuesta" id="9">
										<p>Yes, we work with teams with specialized areas as well as with small teams. We like to be part of a team.</p>
									</div>
								';
							}
						 ?>
						
					</article>
				</section>

		<!-- Footer -->
			<footer id="footwork">
				<section>
					<div class="sociales"><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
					<div class="sociales"><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></div>
					<div class="sociales"><a href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube"></i></a></div>
					<div class="sociales"><a href="https://www.instagram.com/tropicocomedia/" target="_blank"><i class="fab fa-instagram"></i></a></div>
				</section>
			</footer>


		<!-- Scripts -->
		<script src="assets/js/jquery-2.1.4.js"></script>		
		<script src="https://kit.fontawesome.com/14f2e7ba0b.js" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
		<script src="assets/js/sweetalert.js"></script>
		<script type="text/javascript">
			var principal = window.pageYOffset;
			var desplazamiento = 0;
			window.onscroll = function(){
				desplazamiento += window.pageYOffset;
				if (principal>=desplazamiento) {
					document.getElementById("header").style.top = '0';
					document.getElementById("movimiento").style.top = '0';
				}else{
					document.getElementById("header").style.top = '-100px';
					document.getElementById("movimiento").style.top = '-100px';
				}
				principal= desplazamiento;
			}
		</script>
	</body>
</html>