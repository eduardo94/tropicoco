<?php
	session_start();	
	//$_SESSION["valor"]=1;
	$doc = $_SESSION["valor"];
	
?>
<!DOCTYPE html>
<html>
	<head>
		<?php if($doc==1){?>
					<title>INICIO | TROPICOCO MEDIA </title><?php
				}
				else{?>
					<title>HOME | TROPICOCO MEDIA</title><?php
				} ?>
		
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
		<script src="http://code.jquery.com/jquery-latest.js"></script>
		<script src="assets/js/par.js"></script>

	</head>
	<body>
		<!-- Header -->
			<?php 
				if($doc==1){
					include("menue.php");
				}else{
					include("menui.php");
				} 
			?>

		<!-- Contenido -->
			<section id="one"  >
				<section id="banner" class="parallax-window" data-parallax="scroll" data-image-src="../../images/banner.webp">
					<article>
						<?php if($doc=="1"){?>
					<a href="fotos.php"><h4 style="position: relative;float: right;top: 98%">‚‚‚‚ JEUGUEMOS APASIONA</h4></a><?php
				}
				else{?>
					<a href="fotos.php"><h4 style="position: relative;float: right;top: 98%">,,,,LET'S PLAY PASSIONATE</h4></a><?php
				} ?>
					</article>
					
				</section>
			</section>
			<section id="two">
				<video id="vind" src="./video/file.mp4" style="width: 100%"  autoplay="autoplay" loop="loop"  muted></video>
				<?php if($doc=="1"){?>
				<a href="#"><h4 style="top: 94%">,,,,MÁS VIDEOS</h4></a><?php
				}
				else{?>
					<a href="#"><h4 style="top: 94%">,,,,MORE FILMS</h4></a><?php
				} ?>
			</section>
			<section id="three">
				<section id='txtimg'>
					<?php if($doc==1){?>
					<a href="fotos.php"><h4>,,,,MÁS ESTILOS</h4></a><?php
				}
				else{?>
					<a href="fotos.php"><h4>,,,,MORE STILLS</h4></a><?php
				} ?>
				</section>
				<figure><img src="./images/img1.webp" style="width: 100%"></figure>
			</section>
			<footer id="footind">
				<form>
					<?php 
						if ($doc==1) {
							echo '<input type="text" name="email" placeholder="Escribe tu correo">
							<input type="submit" value="Enviar" name="btnEnviar">';
						}else{
							echo '<input type="text" name="email" placeholder="Write your email">
							<input type="submit" value="Send" name="btnEnviar">';
						}
					 ?>
				</form>
				<section>
					<div class="sociales"><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
					<div class="sociales"><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></div>
					<div class="sociales"><a href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube"></i></a></div>
					<div class="sociales"><a href="https://www.instagram.com/tropicocomedia/" target="_blank"><i class="fab fa-instagram"></i></a></div>
				</section>
			</footer>


		<!-- Scripts -->
		<script src="assets/js/jquery-2.1.4.js"></script>		
		<script src="https://kit.fontawesome.com/14f2e7ba0b.js" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
		<script src="assets/js/sweetalert.js"></script>
		<script src="assets/js/parallax.js"></script>
		<script type="text/javascript">
			var principal = window.pageYOffset;
			var desplazamiento = 0;
			window.onscroll = function(){
				desplazamiento += window.pageYOffset;
				if (principal>=desplazamiento) {
					document.getElementById("header").style.top = '0';
					document.getElementById("movimiento").style.top = '0';
				}else{
					document.getElementById("header").style.top = '-100px';
					document.getElementById("movimiento").style.top = '-100px';
				}
				principal= desplazamiento;
			}
		</script>

	</body>
</html>