<?php
	session_start();	
	$doc=$_SESSION["valor"];
?>
<!DOCTYPE html>
<html>
	<head>
		<?php if($doc==1){?>
					<title>NUESTRO TRABAJO | TROPICOCO MEDIA</title><?php
				}
				else{?>
					<title>OUR WORK | TROPICOCO MEDIA</title><?php
				} ?>
		
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@splidejs/splide@1.3.3/dist/css/splide.min.css">

	</head>
	<body>
		<!-- Header -->
			<?php
				if ($doc==1) {
					include "menue.php";
				}else{
					include "menui.php";
				}

			?>

		<!-- Contenido -->
				<section id="fotos" style="padding-top: 7%">

                    <div class="splide crece" id="primary-slider" >
                        <div class="splide__track">
                            <ul class="splide__list">
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">
                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">

                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">

                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">
                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">

                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="splide crece" id="secondary-slider">
                        <div class="splide__track">
                            <ul class="splide__list">
                                <li class="splide__slide">
                                    <img style="width: 100%;height: 500px" src="./images/fondo-films.jpg" alt="">
                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%;height: 500px" src="./images/fondo-films.jpg" alt="">

                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%;height: 500px" src="./images/fondo-films.jpg" alt="">

                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">
                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">

                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">

                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="splide3 crece2" id="primary-slider" >
                        <div class="splide__track">
                            <ul class="splide__list">
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">
                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">

                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">

                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">
                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">

                                </li>
                                <li class="splide__slide">
                                    <img style="width: 100%" src="./images/fondo-films.jpg" alt="">

                                </li>
                            </ul>
                        </div>
                    </div>
				</section>
			<footer id="footwork">
				<section>
					<div class="sociales"><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
					<div class="sociales"><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></div>
					<div class="sociales"><a href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube"></i></a></div>
					<div class="sociales"><a href="https://www.instagram.com/tropicocomedia/" target="_blank"><i class="fab fa-instagram"></i></a></div>
				</section>
			</footer>


		<!-- Scripts -->

		<script src="assets/js/jquery-2.1.4.js"></script>
		<script src="https://kit.fontawesome.com/14f2e7ba0b.js" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
		<script src="assets/js/sweetalert.js"></script>

		<script type="text/javascript">
			var principal = window.pageYOffset;
			var desplazamiento = 0;
			window.onscroll = function(){
				desplazamiento += window.pageYOffset;
				if (principal>=desplazamiento) {
					document.getElementById("header").style.top = '0';
					document.getElementById("movimiento").style.top = '0';
				}else{
					document.getElementById("header").style.top = '-100px';
					document.getElementById("movimiento").style.top = '-100px';
				}
				principal= desplazamiento;
			}
		</script>
        <script src="https://cdn.jsdelivr.net/npm/@splidejs/splide@1.3.3/dist/js/splide.min.js"></script>
        <script>
            var secondarySlider = new Splide( '#secondary-slider', {
                rewind      : true,
                fixedWidth  : 260,
                fixedHeight : 164,
                isNavigation: true,
                gap         : 30,
                focus       : 'center',
                pagination  : false,
                cover       : true,
                breakpoints : {
                    '600': {
                        fixedWidth  : 66,
                        fixedHeight : 40,
                    }
                }
            } ).mount();

            // Create the main slider.
            var primarySlider = new Splide( '#primary-slider', {
                type       : 'fade',
                heightRatio: 0.3,
                pagination : false,
                arrows     : false,
                cover      : true,
            } );

            // Set the thumbnails slider as a sync target and then call mount.
            primarySlider.sync( secondarySlider ).mount();
            new Splide('.splide3').mount();
        </script>
	</body>
</html>