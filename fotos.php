<?php
	session_start();	
	$doc=$_SESSION["valor"];
?>
<!DOCTYPE html>
<html>

	<head>
		<?php if($doc=="1"){?>
					<title>FOTOGRAFÍA | TROPICOCO MEDIA</title><?php
				}
				else{?>
					<title>PHOTOGRAPHY | TROPICOCO MEDIA</title><?php
				} ?>
		
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />

	</head>
	<body>
		<!-- Header -->
			<?php if($doc==1){
					include "menue.php";
				}
				else{
					include "menui.php";
				} ?>


		<!-- Contenido -->
			<section id="contenedoralb">
				<section id="contfotos">
					<div class="fts">
						<a href="./collect1.php"><figure><img src="./images/alb1.webp"></figure></a>
					</div>
					<div class="fts">
						<a href="./collect2.php"><figure><img src="./images/alb2.webp"></figure></a>
					</div>
				</section>
				<section id="contfotos">
					<div class="fts">
						<a href="./collect3.php"><figure><img src="./images/alb3.webp"></figure></a>
					</div>
					<div class="fts">
						<a href="./collect4.php"><figure><img src="./images/alb4.webp"></figure></a>
					</div>
				</section>
				<section id="contfotos">
					<div class="fts">
						<a href="./collect5.php"><figure><img src="./images/alb5.webp"></figure></a>
					</div>
					<div class="fts">
						<a href="./collect6.php"><figure><img src="./images/alb6.webp"></figure></a>
					</div>
				</section>
				<section id="contfotos">
					<div class="fts">
						<a href="collect7.php"><figure><img src="./images/alb7.webp"></figure></a>
					</div>
					<div class="fts">
						<a href="collect8.php"><figure><img src="./images/alb8.webp"></figure></a>
					</div>
				</section>
			</section>


			<footer id="footwork">
				<section>
					<div class="sociales"><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
					<div class="sociales"><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></div>
					<div class="sociales"><a href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube"></i></a></div>
					<div class="sociales"><a href="https://www.instagram.com/tropicocomedia/" target="_blank"><i class="fab fa-instagram"></i></a></div>
				</section>
			</footer>


		<!-- Scripts -->
		<script src="assets/js/jquery-2.1.4.js"></script>
		<script src="https://kit.fontawesome.com/14f2e7ba0b.js" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
		<script src="assets/js/sweetalert.js"></script>

		<script type="text/javascript">
			var principal = window.pageYOffset;
			var desplazamiento = 0;
			window.onscroll = function(){
				desplazamiento += window.pageYOffset;
				if (principal>=desplazamiento) {
					document.getElementById("header").style.top = '0';
					document.getElementById("movimiento").style.top = '0';
				}else{
					document.getElementById("header").style.top = '-100px';
					document.getElementById("movimiento").style.top = '-100px';
				}
				principal= desplazamiento;



			}
		</script>
	</body>
</html>