function Request(){
	if (window.XMLHttpRequest) {
		return new XMLHttpRequest();
	}
	else if (window.ActiveXObject) {
		var versionObj= new Array(
			'Msxml2.XMLHTTP.5.0',
			'Msxml2.XMLHTTP.4.0',
			'Msxml2.XMLHTTP.3.0',
			'Msxml2.XMLHTTP',
			'Microsoft.XMLHTTP');
		for (var i = 0; i < versionObj.length; i++) {
			try	{
				return new ActiveXObject(versionObj[i]);
			}
			catch(errorControlado){

			}
		}
	}
	throw new Error("No se pudo crear el objeto HttpRequest");
}