<?php
	$doc="1";
?>
<!DOCTYPE html>
<html>
	<head>
		<?php if($doc=="1"){?>
					<title>CONTACTO | TROPICOCO MEDIA</title><?php
				}
				else{?>
					<title>CONTACT | TROPICOCO MEDIA</title><?php
				} ?>
		
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link href="https://fonts.googleapis.com/css2?family=Caveat&display=swap" rel="stylesheet">
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>
		<!-- Header -->
			<?php if($doc==1){
					include "menue.php";
				}
				else{
					include "menui.php";
				} ?>

		<!-- Contenido -->
				<section id="contact" style="padding-top: 7%">
					
					
						<div style="width: 100%"><iframe width="100%" height="600" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps?width=100%25&amp;height=600&amp;hl=es&amp;q=merida+(Mi%20nombre%20de%20egocios)&amp;t=&amp;z=14&amp;ie=UTF8&amp;iwloc=B&amp;output=embed"></iframe></div>
				
				</section>
			<footer id="footwork">
				<section>
					<div class="sociales"><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
					<div class="sociales"><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></div>
					<div class="sociales"><a href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube"></i></a></div>
					<div class="sociales"><a href="https://www.instagram.com/tropicocomedia/" target="_blank"><i class="fab fa-instagram"></i></a></div>
				</section>
			</footer>


		<!-- Scripts -->
		<script src="assets/js/jquery-2.1.4.js"></script>		
		<script src="https://kit.fontawesome.com/14f2e7ba0b.js" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
		<script src="assets/js/sweetalert.js"></script>
		<script type="text/javascript">
			var principal = window.pageYOffset;
			var desplazamiento = 0;
			window.onscroll = function(){
				desplazamiento += window.pageYOffset;
				if (principal>=desplazamiento) {
					document.getElementById("header").style.top = '0';
					document.getElementById("movimiento").style.top = '0';
				}else{
					document.getElementById("header").style.top = '-100px';
					document.getElementById("movimiento").style.top = '-100px';
				}
				principal= desplazamiento;
			}
		</script>
	</body>
</html>