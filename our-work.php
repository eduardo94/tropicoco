<?php
	session_start();	
	$doc=$_SESSION["valor"];
?>
<!DOCTYPE html>
<html>
	<head>
		<?php if($doc==1){?>
					<title>NUESTRO TRABAJO | TROPICOCO MEDIA</title><?php
				}
				else{?>
					<title>OUR WORK | TROPICOCO MEDIA</title><?php
				} ?>
		
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
		<link rel="stylesheet" href="assets/css/main.css" />
	</head>
	<body>
		<!-- Header -->
			<?php 
				if ($doc==1) {
					include "menue.php";
				}else{
					include "menui.php";
				}
				
			?>

		<!-- Contenido -->
				<section id="fotos" style="padding-top: 7%">
					<div class="fts">
						<a href="fotos.php" id="mv1"><figure><img src="./images/work1.webp"></figure>
						<?php if($doc=="1"){?>
							<h4>,,,MÁS ESTILOS</h4><?php
							}
							else{?>
								<h4>,,,,MORE STILLS</h4><?php
						} ?>
					</div>
					<div class="fts">
						<a href="films.php" id="mv2"><figure><img src="./images/work2.webp"></figure>
						<?php if($doc=="1"){?>
							<h4>,,,MÁS VIDEOS</h4><?php
							}
							else{?>
								<h4>,,,,MORE FILMS</h4><?php
						} ?>
					</div>
				</section>
			<footer id="footwork">
				<section>
					<div class="sociales"><a href="https://www.facebook.com/" target="_blank"><i class="fab fa-facebook-f"></i></a></div>
					<div class="sociales"><a href="https://twitter.com/" target="_blank"><i class="fab fa-twitter"></i></a></div>
					<div class="sociales"><a href="https://www.youtube.com/" target="_blank"><i class="fab fa-youtube"></i></a></div>
					<div class="sociales"><a href="https://www.instagram.com/tropicocomedia/" target="_blank"><i class="fab fa-instagram"></i></a></div>
				</section>
			</footer>


		<!-- Scripts -->
		<script src="assets/js/jquery-2.1.4.js"></script>		
		<script src="https://kit.fontawesome.com/14f2e7ba0b.js" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
		<script src="assets/js/sweetalert.js"></script>
		<script type="text/javascript">
			var principal = window.pageYOffset;
			var desplazamiento = 0;
			window.onscroll = function(){
				desplazamiento += window.pageYOffset;
				if (principal>=desplazamiento) {
					document.getElementById("header").style.top = '0';
					document.getElementById("movimiento").style.top = '0';
				}else{
					document.getElementById("header").style.top = '-100px';
					document.getElementById("movimiento").style.top = '-100px';
				}
				principal= desplazamiento;
			}
		</script>
	</body>
</html>